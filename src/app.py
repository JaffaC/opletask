from flask import Flask, jsonify, request
from sqlalchemy import create_engine
from machine_learning import train_model
from data_access.database import db_session
from utils import config
from models import Experiment

db_connect = create_engine(config.DB_CONNECTION_STRING)
app = Flask(__name__)


@app.route('/experiments/<experiment_id>')
def get_by_id(experiment_id):
    conn = db_connect.connect()
    query = conn.execute("select * from experiments where id = " + experiment_id)
    result = {'data': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
    return jsonify(result), 200


@app.route('/experiments', methods=['POST'])
def post():
    req_data = request.get_json()
    name = req_data['name']
    experiment_type = req_data['type']
    e = Experiment(name, experiment_type)
    db_session.add(e)
    db_session.commit()
    return str(e.id), 201


@app.route('/experiments/<experiment_id>', methods=['PUT'])
def update(experiment_id):
    req_data = request.get_json()
    name = req_data['name']
    experiment_type = req_data['type']

    experiment = Experiment.query.get(experiment_id)
    experiment.name = name
    experiment.experiment_type = experiment_type

    db_session.commit()

    return "", 204


@app.route('/experiments/<experiment_id>', methods=['DELETE'])
def delete(experiment_id):
    db_session.delete(Experiment.query.get(experiment_id))
    db_session.commit()
    return "", 202


@app.route('/experiments/train/<experiment_id>', methods=['POST'])
def train(experiment_id):
    train_data_file = request.files['train_data']
    train_data = train_data_file.read()
    experiment = Experiment.query.get(experiment_id)
    experiment.train_data = train_data
    db_session.commit()
    return "", 200


@app.route('/experiments/test/<experiment_id>', methods=['POST'])
def test(experiment_id):
    test_data_file = request.files['test_data']
    test_data = test_data_file.read()
    experiment = Experiment.query.get(experiment_id)
    experiment.test_data = test_data
    db_session.commit()
    return "", 200


@app.route('/experiments/predict/<experiment_id>', methods=['GET'])
def predict(experiment_id):
    experiment = Experiment.query.get(experiment_id)
    experiment.result = train_model.run(experiment.train_data, experiment.test_data)
    db_session.commit()
    return experiment.result, 200


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == '__main__':
    app.run(port=5002, debug=True)

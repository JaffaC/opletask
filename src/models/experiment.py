from sqlalchemy import Column, Integer, String, TIMESTAMP, Text
from data_access.database import Base


class Experiment(Base):
    __tablename__ = 'experiments'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    start_date = Column(TIMESTAMP)
    experiment_type = Column('type', String(120))
    result = Column(String(50))
    test_data = Column(Text)
    train_data = Column(Text)

    def __init__(self, name=None, experiment_type=None, start_date=None, result=None, test_date=None, train_data=None):
        self.name = name
        self.experiment_type = experiment_type
        self.result = result
        self.start_date = start_date
        self.test_data = test_date
        self.train_data = train_data

    def __repr__(self):
        return '<Experiment %r>' % self.name

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

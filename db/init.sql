CREATE DATABASE ople;
use ople;

CREATE TABLE `experiments` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` CHAR(50) NOT NULL DEFAULT '0',
	`start_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`type` CHAR(50) NOT NULL DEFAULT '0',
	`result` VARCHAR(50) NULL,
	`test_data` TEXT NULL,
	`train_data` TEXT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;

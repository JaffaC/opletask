OpleTask

Set up instructions

Database

Setting a local database
1.  Open folder db
2.  Run - 'db/create_db.sql' to create database on the DB of your choice. Development was done using MySQL
3.  Run - 'db/experiments.sql' to create the experiments table.
4.  Open - 'utils/config.py' set the proper database credentials in DB_CONNECTION_STRING key. 
    Check - 'http://docs.sqlalchemy.org/en/latest/dialects/' to set up the proper dialect.
    
Docker

1.  Open a terminal and navigate to the root directory.
2.  Build and launch the docker by calling - docker-compose up

Usage

1. Run bash script - 'walkthrough.sh' to do the following steps
    a.  Create an experiment.
    b.  Update the experiment.
    c.  Fetch the experiment by id.
    d.  Upload experiment train data.
    e.  Upload experiment test data.
    f.  Calculate experiment prediction.
    g.  (commented)Delete the experiment.




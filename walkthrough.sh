#!/usr/bin/env bash

# generate a unique timestamp for the experiment name.
timestamp=$(date +%Y-%m-%dT%H:%M:%S)

# create the experiment.
newId=$(curl -b POST -H "Content-type: application/json" http://127.0.0.1:5002/experiments -d '{"name":"experiment # '"$timestamp"'", "type":"sales"}')

# update the experiment name and type.
curl \
--header "Content-type: application/json" \
--request PUT \
--data '{"name":"experiment # '"$timestamp"' revised", "type":"sales prediction"}' \
http://127.0.0.1:5002/experiments/${newId}

# fetch the experiment by id.
curl \
--request GET \
http://127.0.0.1:5002/experiments/${newId}

# upload experiment training data.
curl -X POST -F 'train_data=@data/sales_data_training.csv' http://127.0.0.1:5002/experiments/train/${newId}

# upload experiment test data.
curl -X POST -F 'test_data=@data/sales_data_test.csv' http://127.0.0.1:5002/experiments/test/${newId}

# calculate experiment prediction.
curl \
--request GET \
http://127.0.0.1:5002/experiments/predict/${newId}

# delete experiment.
#curl \
#--request DELETE \
#http://127.0.0.1:5002/experiments/${newId}




